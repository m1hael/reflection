     /*********************************************************************/
     /*                                                                   */
     /* Header File Name: errords                                         */
     /* Standard Declaration Error DS for APIs                            */
     /*                                                                   */
     /*********************************************************************/
      /IF NOT DEFINED (ERRORDS_QRPGLEH)
      /DEFINE ERRORDS_QRPGLEH
     /*===================================================================*/
     DErrorDS          DS
     D BytesProvided                 10I 0 inz(%size(ErrParms))
     D BytesAvailable                10I 0
     D MessageId                      7A
     D ErrReserved                    1A
     D ErrParms                     128A
      /ENDIF

