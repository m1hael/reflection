     /************************************************************************/
     /* File: reflection.rpgle                                               */
     /* exports: GetProcedurePointer                                         */
     /*             gets Name of Procedure and its Serviceprogram            */
     /*             returns procedure pointer to the activation              */
     /*             of the procedure, wich can be used for procedure call    */
     /*             without the need of binding it                           */
     /*             example in TSTREFLECT.QRPGLESRC                          */
     /* Copyright (C) 2002  Dieter Bender  <db@bender-dv.de>                 */
     /*                                                                      */
     /* This program is free software; you can redistribute it and/or modify */
     /* it under the terms of the GNU General Public License as published by */
     /* the Free Software Foundation.                                        */
     /*                                                                      */
     /* This program is distributed in the hope that it will be useful,      */
     /* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
     /* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
     /* GNU General Public License for more details.                         */
     /*                                                                      */
     /* You should have received a copy of the GNU General Public License    */
     /* along with this program; if not, write to the Free Software          */
     /* Foundation, Inc., 59 Temple Place,                                   */
     /* Suite 330, Boston, MA  02111-1307   USA                              */
     /* You might find a version at http://www.gnu.org                       */
     /************************************************************************/

     H copyright('Dieter Bender 08/2002 <db@bender-dv-de>')
     H nomain
     
      /if defined(THREAD_SAFE)
     H THREAD(*CONCURRENT)
      /endif

      /include 'reflection_h.rpgle'
      /include 'qlicvttp_h.rpgle'
      /include 'resolvePointer_h.rpgle'
      /include 'qleawi_h.rpgle'
      /include 'errords.rpgle'

     P reflection_getProcedurePointer...
     P                 B                   export
     D                 PI              *   procptr
     D  servicePgm                   10A   const
     D  procedure                   256A   const
     /*------------------------------------------------------------*/
     D result          S               *   procptr
     D charType        S             10A   inz('*SRVPGM')
     D hexType         S              2A
     D servicePgmP     S               *   procptr
     D activateResult  S             10i 0
     D activationMark  S             10i 0
     D activationInfo  S                   like(Qle_ABP_Info_t)
     D actInfoLen      S             10i 0 inz(%size(ActivationInfo))
     D exportType      S             10i 0
     D error           DS                  likeds(ErrorDS) inz
      /free
       convertObjectTypeHex('*SYMTOHEX' : charType : hexType : error);

       servicePgmP = getSysPointer(hexType:%trim(servicePgm):'*LIBL':AUTH_NONE);

       activateResult = activateProgram( servicePgmP : activationMark :
                                         activationInfo : actInfolen  : error);

       getExport(activationMark : 0 : %len(%trim(procedure)): %trim(procedure):
                 result : exportType : error);

       if (exportType <> QLE_EX_PROC);
         result = *null;
       endif;

       return result;
      /end-free
     P                 E
