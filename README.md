# Reflection

This project contains the procedure to get a pointer to a procedure simply by 
its name.


## Code sample

```
procedurePointer = reflection_getProcedurePointer(srvpgm : procedureName);
```


## Requirements

This software has no further dependencies. It comes with all necessary files.


## Installation
For standard installation the setup script can be executed as is. This will 
build the service program in the library *OSSILE*. If you want to build the
service program in any other library export the library name in the variable
`TARGET_LIB` like this

    export TARGET_LIB=MSCHMIDT

before executing the *setup* script.

For automatically copying the copybook to a directory in the IFS export 
`INCDIR` like this

    export INCDIR=/usr/local/include/

before executing the setup script. The directory which is stated in the export
should exist before executing the script.


## Documentation

The API documentation is be available at [ILEDocs](http://iledocs.rpgnextgen.com) 
hosted at rpgnextgen.com.

## License

This software is released under the [GPL](https://www.gnu.org/licenses/gpl.html) License.
